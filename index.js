(function () {
	function onload(containerId, userId, portfolioId) {
		const container = document.getElementById(containerId);

		let node = document.createElement("h1");
		node.append(document.createTextNode("It works!"));
		container.appendChild(node);
	
		node = document.createElement("pre");
		node.append(document.createTextNode(`userId: ${userId}\nportfolioId: ${portfolioId}`));
		container.appendChild(node);
	}
	
	registerWOPlugin("SecuritiesRecommendationEngine",
		containerId => onload(
			containerId,
			WOPluginInfoProvider.currentUserId(),
			WOPluginInfoProvider.currentPortfolioId(),
		),
	);
})();
